import requests
import re

class pcloud_api:
    base_url = "http://api.pcloud.com/"
    username = ""
    password = ""
    
    def getAuth(self):
        return "&username="+self.username+"&password="+self.password
    
    def list_folders(self,path):
        url = self.base_url + "listfolder?path="+path+""+self.getAuth()
        url = re.sub(r'\\','/',url)
        r = requests.get(url)
        print url
        return r.json()['metadata']
    
    def check_user(self):
        url = self.base_url + "userinfo?getauth=1"+self.getAuth()
        r = requests.get(url)
        return r.json()
        
    def set_auth(self,username,passwrd):
        self.username = username.strip()
        self.password = passwrd.strip()
        
    def create_dir(self,path,ime):
        url = self.base_url + "createfolder?path="+path+"&name="+ime+""+self.getAuth()
        url = re.sub(r'\\','/',url)
        r = requests.get(url)
        return r.json()
    
    def delete_dir(self,path):
        url = "deletefolder?path="+path
        return self.makeReq(url)


    def makeReq(self,url):
        url = self.base_url + url +self.getAuth()
        url = re.sub(r'\\','/',url)
        r = requests.get(url)
        return r.json()

    def get_download_url(self,path):
        url = "getfilelink?path="+path
        r = self.makeReq(url)
        url = 'http://'+r['hosts'][0] + r['path']
        return url
        print r
        
    def upload_file(self,chunk):
        pass