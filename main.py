#!/usr/bin/env python2
# coding: utf-8

import os, socket, threading
import pcloud_api
import urllib2


api = pcloud_api.pcloud_api()

allow_delete = False
local_ip = socket.gethostbyname(socket.gethostname())
local_port = 8888
currdir = os.path.abspath('.')

class FTPserverThread(threading.Thread):
    def __init__(self, (conn, addr)):
        self.username = ""
        self.password = ""
        self.conn = conn
        self.addr = addr
        self.basewd = "/"
        self.cwd = self.basewd
        self.previus_dir = "/"
        self.rest = False
        self.pasv_mode = False
        threading.Thread.__init__(self)

    def run(self):
        self.conn.send('220 Welcome! to pCloud ftp server\r\n')
        while True:
            cmd = self.conn.recv(256)
            if not cmd: break
            else:
                print 'Recieved:', cmd
                try:
                    func = getattr(self, cmd[:4].strip().upper())
                    func(cmd)
                except Exception, e:
                    print 'ERROR:', e
                    # traceback.print_exc()
                    self.conn.send('500 Sorry.\r\n')

    def SYST(self, cmd):
        self.conn.send('215 UNIX Type: L8\r\n')
    def OPTS(self, cmd):
        if cmd[5:-2].upper() == 'UTF8 ON':
            self.conn.send('200 OK.\r\n')
        else:
            self.conn.send('451 Sorry.\r\n')
    def USER(self, cmd):
        self.username = cmd.replace("USER ","")
        self.conn.send('331 OK.\r\n')
    def PASS(self, cmd):
        self.password = cmd.replace("PASS ","")
        api.set_auth(self.username,self.password)
        res = api.check_user()
        if res['result'] == 0:
            self.conn.send('230 OK.\r\n')
        else:
            self.conn.send('530 '+res['error']+'\r\n')
   
    def QUIT(self, cmd):
        self.conn.send('221 Goodbye.\r\n')
    def NOOP(self, cmd):
        self.conn.send('200 OK.\r\n')
    def TYPE(self, cmd):
        self.mode = cmd[5]
        self.conn.send('200 Binary mode.\r\n')

    def CDUP(self, cmd):
        print "!? WHAT ?!"
        self.cwd = self.previus_dir
        self.conn.send('200 OK.\r\n')
    def PWD(self, cmd):
        cwd = os.path.relpath(self.cwd, self.basewd)
        if cwd == '.':
            cwd = '/'
        else:
            cwd = '/' + cwd
        self.conn.send('257 \"%s\"\r\n' % cwd)
    def CWD(self, cmd):
        chwd = cmd[4:-2]
        if chwd == '/':
            self.cwd = self.basewd
        elif chwd[0] == '/':
            self.cwd = os.path.join(self.basewd, chwd[1:])
        else:
            self.cwd = os.path.join(self.cwd, chwd)
        self.conn.send('250 OK.\r\n')

    def PORT(self, cmd):
        if self.pasv_mode:
            self.servsock.close()
            self.pasv_mode = False
        l = cmd[5:].split(',')
        self.dataAddr = '.'.join(l[:4])
        self.dataPort = (int(l[4]) << 8) + int(l[5])
        self.conn.send('200 Get port.\r\n')

    def PASV(self, cmd): 
        self.pasv_mode = True
        self.servsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.servsock.bind((local_ip, 0))
        self.servsock.listen(1)
        ip, port = self.servsock.getsockname()
        print 'open', ip, port
        self.conn.send('227 Entering Passive Mode (%s,%u,%u).\r\n' % 
                (','.join(ip.split('.')), port >> 8 & 0xFF, port & 0xFF))

    def start_datasock(self):
        if self.pasv_mode:
            self.datasock, addr = self.servsock.accept()
            print 'connect:', addr
        else:
            self.datasock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.datasock.connect((self.dataAddr, self.dataPort))

    def stop_datasock(self):
        self.datasock.close()
        if self.pasv_mode:
            self.servsock.close()


    def LIST(self, cmd):
        self.conn.send('150 Directory listing.\r\n')
        print 'list:', self.cwd
        self.start_datasock()
        #
        if self.previus_dir == "/":
            self.previus_dir = ""
        folders =  api.list_folders(self.cwd)
        self.previus_dir = folders['path']
        for el in folders['contents']:
            if el["isfolder"] == True:
                k = self.toListDirs2(el)
                self.datasock.send(k + '\r\n')
            else:
                k = self.toListItem(el)
                self.datasock.send(k + '\r\n')
        self.stop_datasock()
        self.conn.send('226 Directory send OK.\r\n')

    def toListDirs2(self,el):
        return "drwxrwxrwx 1 user group 0 Dec 07 09:17 " + el["name"] + "/"


    def toListItem(self, el):
        return "-rw-r--r-- 1 "+self.username.strip()+" "+self.username.strip()+" "+str(el['size'])+" Nov 18 11:26 " + el['name']

    def MKD(self, cmd):
        ime = cmd[4:-2]
        dn = os.path.join(self.cwd, cmd[4:-2])
        res = api.create_dir(dn,ime)
        if res['result'] == 0:
            self.conn.send('257 Directory created.\r\n')
        else:
            self.conn.send('450 '+ res['error'] +'.\r\n')

    def RMD(self, cmd):
        dn = os.path.join(self.cwd, cmd[4:-2])
        r = api.delete_dir(dn)
        if r['result'] == 0:
            self.conn.send('250 Directory deleted.\r\n')
        else:
            self.conn.send('450 '+r['error']+'.\r\n')

    def DELE(self, cmd):
#       fn = os.path.join(self.cwd, cmd[5:-2])
#       self.conn.send('250 File deleted.\r\n')  --> soon
        self.conn.send('450 Not allowed.\r\n')

    def RNFR(self, cmd):
        self.rnfn = os.path.join(self.cwd, cmd[5:-2])
        self.conn.send('350 Ready.\r\n')

    def RNTO(self, cmd):
#         fn = os.path.join(self.cwd, cmd[5:-2]) --> soon
        self.conn.send('250 File renamed.\r\n')

    def REST(self, cmd):
        self.pos = int(cmd[5:-2])
        self.rest = True
        self.conn.send('250 File position reseted.\r\n')

    def RETR(self, cmd):
        fn = os.path.join(self.cwd, cmd[5:-2])
        url = api.get_download_url(fn)
        self.conn.send('150 Opening data connection.\r\n')
        req = urllib2.urlopen(url)
        CHUNK = 1024
        self.start_datasock()
        while True:
            chunk = req.read(CHUNK)
            if not chunk: break
            self.datasock.send(chunk)
        self.stop_datasock()
        self.conn.send('226 Transfer complete.\r\n')
        
        
    def STOR(self, cmd):
        fn = os.path.join(self.cwd, cmd[5:-2])
        print 'Uplaoding:', fn
        name =  cmd[5:-2]
        
        self.conn.send('150 Opening data connection.\r\n')
        self.start_datasock()
        while True:
            data = self.datasock.recv(1024)
            if not data: break
#             write(data)

        self.stop_datasock()
        self.conn.send('226 Transfer complete.\r\n')

class FTPserver(threading.Thread):
    def __init__(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind((local_ip, local_port))
        threading.Thread.__init__(self)

    def run(self):
        self.sock.listen(5)
        while True:
            th = FTPserverThread(self.sock.accept())
            th.daemon = True
            th.start()

    def stop(self):
        self.sock.close()

if __name__ == '__main__':
    ftp = FTPserver()
    ftp.daemon = True
    ftp.start()
    print 'On', local_ip, ':', local_port
    raw_input('Enter to end...\n')
    ftp.stop()
